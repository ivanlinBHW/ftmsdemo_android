package com.example.ftmsDemo;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.le.ScanResult;
import android.os.Bundle;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.example.ftmsDemo.model.DataRecordIndoorBike;
import com.example.ftmsDemo.model.IndoorBikeData;
import com.example.ftmsDemo.util.ConnectManager;
import com.example.ftmsDemo.util.Helper;
import com.example.ftmsDemo.util.WriteCommand;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class ConnectedActivity extends AppCompatActivity {
    private ScanResult blueToothScanResult;
    private TextView distance_txt, rpm_txt, watt_txt;
    private List<WriteCommand> commandList = new ArrayList<WriteCommand>();
    private List<BluetoothGattCharacteristic> characteristicSupportedArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connected);
        distance_txt = findViewById(R.id.distance_txt);
        rpm_txt = findViewById(R.id.rpm_txt);
        watt_txt = findViewById(R.id.watt_txt);

        IndoorBikeData.getInstance().init();
        characteristicSupportedArray = new ArrayList<BluetoothGattCharacteristic>();

        if (ConnectManager.currentDevice == null || ConnectManager.currentDevice.getScanResult().getDevice() == null) {
            finish();
            return;
        }

        setBlueToothScanResult(ConnectManager.currentDevice.getScanResult());
        startConnect();

    }


    private void setBlueToothScanResult(ScanResult d) {
        blueToothScanResult = d;
        Helper.log("Device ===" + blueToothScanResult);
    }

    public void startConnect() {
        Helper.log("Device ===" + ConnectManager.currentDevice.getScanResult());
        ConnectManager.ConnectManagerListener connectManagerListener = new ConnectManager.ConnectManagerListener() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                if (newState == BluetoothGatt.STATE_CONNECTED) {
                    gatt.discoverServices();
                } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
                    gatt.close();
                    finish();
                }
            }

            @Override
            public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                if (status != BluetoothGatt.GATT_SUCCESS) {
                    finish();
                    return;
                }
            }

            @Override
            public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

            }

            @Override
            public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                if (characteristic.getUuid().equals(ConnectManager.INDOOR_BIKE_DATA)) {

                    DataRecordIndoorBike dataRecordIndoorBike = new DataRecordIndoorBike(characteristic);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DecimalFormat df = new DecimalFormat("0.0");
                            DecimalFormat df2 = new DecimalFormat("0.00");
                            if (DataRecordIndoorBike.data.getTotalDistance().getEnabled())
                                distance_txt.setText("Distance \n " + String.valueOf(df2.format(DataRecordIndoorBike.data.getTotalDistance().getValue() / 1000)) + "km");
                            if (DataRecordIndoorBike.data.getTotalEnergy().getEnabled())
                                rpm_txt.setText("RPM \n " + String.valueOf((int) DataRecordIndoorBike.data.getInstantaneousCadence().getValue()));
                            if (DataRecordIndoorBike.data.getInstantaneousPower().getEnabled())
                                watt_txt.setText("Watt \n " + String.valueOf((int) DataRecordIndoorBike.data.getInstantaneousPower().getValue()));
                        }
                    });

                }
            }

            @Override
            public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {

            }

            @Override
            public void onFinishedConnect(BluetoothGatt gatt, int newState) {

            }
        };
        ConnectManager.getInstance().startConnect(ConnectedActivity.this, blueToothScanResult, ConnectManager.currentDevice, connectManagerListener);
    }

    private void disconnectAll() {
        DataRecordIndoorBike.isPaused = false;
        DataRecordIndoorBike.startCount = false;
        if (ConnectManager.currentDevice == null) return;
        if (ConnectManager.currentDevice.getScanResult() == null) return;
        ConnectManager.getInstance().disconnectAll();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        disconnectAll();
        super.onDestroy();
    }

}
