package com.example.ftmsDemo.model;

import com.example.ftmsDemo.util.BaseUtils;

public class IndoorBikeData {


    public static InstantaneousSpeed instantaneousSpeed;
    public static AverageSpeed averageSpeed;
    public static InstantaneousCadence instantaneousCadence;
    public static AverageCadence averageCadence;
    public static TotalDistance totalDistance;
    public static ResistanceLevel resistanceLevel;
    public static InstantaneousPower instantaneousPower;
    public static AveragePower averagePower;
    public static TotalEnergy totalEnergy;
    public static EnergyPerHour energyPerHour;
    public static EnergyPerMinute energyPerMinute;
    public static HeartRate heartRate;
    public static MetabolicEquivalent metabolicEquivalent;
    public static ElapsedTime elapsedTime;
    public static RemainingTime remainingTime;

    private static class SingletonHolder {
        private static final IndoorBikeData INSTANCE = new IndoorBikeData();
    }

    public static final IndoorBikeData getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public IndoorBikeData() {
        init();
    }

    public void init() {
        instantaneousSpeed = new InstantaneousSpeed();
        averageSpeed = new AverageSpeed();
        instantaneousCadence = new InstantaneousCadence();
        averageCadence = new AverageCadence();
        totalDistance = new TotalDistance();
        resistanceLevel = new ResistanceLevel();
        instantaneousPower = new InstantaneousPower();
        averagePower = new AveragePower();
        totalEnergy = new TotalEnergy();
        energyPerHour = new EnergyPerHour();
        energyPerMinute = new EnergyPerMinute();
        heartRate = new HeartRate();
        metabolicEquivalent = new MetabolicEquivalent();
        elapsedTime = new ElapsedTime();
        remainingTime = new RemainingTime();

    }

    public static class InstantaneousSpeed {
        private float value;
        private String name = "Instantaneous Speed";
        private String unit = "kph";
        public static String displayUnit = "kph";  //mile/h
        public static String displayUnitPace = "min/km";  //min/mile

        private float resolution = 0.01f;
        private int length = BaseUtils.UINT16;
        private static boolean enabled;

        public float getResolution() {
            return resolution;
        }

        public String getName() {
            return name;
        }

        public void setValue(float val) {
            this.value = val;
        }

        public float getRealValue() {
            return this.value;
        }

        public float getValue() {
                return this.value;
        }

        public int getLength() {
            return this.length;
        }

        public void setEnabled(Boolean status) {
            this.enabled = status;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

    }

    public void setInstantaneousSpeed(InstantaneousSpeed instantaneousSpeed) {
        this.instantaneousSpeed = instantaneousSpeed;
    }

    public InstantaneousSpeed getInstantaneousSpeed() {
        return instantaneousSpeed;
    }


    public static class AverageSpeed {
        private float value;
        private String name = "Average Speed";
        private String unit = "km/h";
        private float resolution = 0.01f;
        private int length = BaseUtils.UINT16;
        private boolean enabled;

        public float getResolution() {
            return resolution;
        }

        public String getName() {
            return name;
        }

        public void setValue(float val) {
            this.value = val;
        }

        public float getValue() {
            return this.value;
        }

        public int getLength() {
            return this.length;
        }

        public void setEnabled(Boolean status) {
            this.enabled = status;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

    }

    public void setAverageSpeed(AverageSpeed averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public AverageSpeed getAverageSpeed() {
        return averageSpeed;
    }


    public static class InstantaneousCadence {
        private float value;
        private String name = "Instantaneous Cadence";
        private String unit = "rpm";
        private float resolution = 0.5f;
        private int length = BaseUtils.UINT16;
        private boolean enabled;

        public float getResolution() {
            return resolution;
        }

        public String getName() {
            return name;
        }

        public void setValue(float val) {
            this.value = val;
        }

        public float getValue() {
            return this.value;
        }

        public int getLength() {
            return this.length;
        }

        public void setEnabled(Boolean status) {
            this.enabled = status;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

    }

    public void setInstantaneousCadence(InstantaneousCadence instantaneousCadence) {
        this.instantaneousCadence = instantaneousCadence;
    }

    public InstantaneousCadence getInstantaneousCadence() {
        return instantaneousCadence;
    }


    public static class AverageCadence {
        private float value;
        private String name = "Average Cadence";
        private String unit = "rpm";
        private float resolution = 0.5f;
        private int length = BaseUtils.UINT16;
        private boolean enabled;

        public float getResolution() {
            return resolution;
        }

        public String getName() {
            return name;
        }

        public void setValue(float val) {
            this.value = val;
        }

        public float getValue() {
            return this.value;
        }

        public int getLength() {
            return this.length;
        }

        public void setEnabled(Boolean status) {
            this.enabled = status;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

    }

    public void setAverageCadence(AverageCadence averageCadence) {
        this.averageCadence = averageCadence;
    }

    public AverageCadence getAverageCadence() {
        return averageCadence;
    }


    public static class TotalDistance {
        private float value;
        private String name = "Total Distance";
        private String unit = "m";
        public static String displayUnit = "km/h";  //mile/h
        private float resolution = 1.0f;
        private int length = BaseUtils.UINT24;
        private boolean enabled;

        public float getResolution() {
            return resolution;
        }

        public String getName() {
            return name;
        }

        public void setValue(float val) {
            this.value = val;
        }

        public float getRealValue() {
            return this.value;
        }

        public float getValue() {
                return this.value;
        }

        public int getLength() {
            return this.length;
        }

        public void setEnabled(Boolean status) {
            this.enabled = status;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

    }

    public void setTotalDistance(TotalDistance totalDistance) {
        this.totalDistance = totalDistance;
    }

    public TotalDistance getTotalDistance() {
        return totalDistance;
    }


    public static class ResistanceLevel {
        private float value;
        private String name = "Resistance Level";
        private String unit = "";
        private float resolution = 1.0f;
        private int length = Math.abs(BaseUtils.SINT16);
        private boolean enabled;

        public float getResolution() {
            return resolution;
        }

        public String getName() {
            return name;
        }

        public void setValue(float val) {
            this.value = val;
        }

        public float getValue() {
            return this.value;
        }

        public int getLength() {
            return this.length;
        }

        public void setEnabled(Boolean status) {
            this.enabled = status;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

    }

    public void setResistanceLevel(ResistanceLevel resistanceLevel) {
        this.resistanceLevel = resistanceLevel;
    }

    public ResistanceLevel getResistanceLevel() {
        return resistanceLevel;
    }


    public static class InstantaneousPower {
        private float value;
        private String name = "Instantaneous Power";
        private String unit = "watts";
        private float resolution = 1.0f;
        private int length = Math.abs(BaseUtils.SINT16);
        private boolean enabled;

        public float getResolution() {
            return resolution;
        }

        public String getName() {
            return name;
        }

        public void setValue(float val) {
            this.value = val;
        }

        public float getValue() {
            return this.value;
        }

        public int getLength() {
            return this.length;
        }

        public void setEnabled(Boolean status) {
            this.enabled = status;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

    }

    public void setInstantaneousPower(InstantaneousPower instantaneousPower) {
        this.instantaneousPower = instantaneousPower;
    }

    public InstantaneousPower getInstantaneousPower() {
        return instantaneousPower;
    }


    public static class AveragePower {
        private float value;
        private String name = "Average Power";
        private String unit = "watts";
        private float resolution = 1.0f;
        private int length = Math.abs(BaseUtils.SINT16);
        private boolean enabled;

        public float getResolution() {
            return resolution;
        }

        public String getName() {
            return name;
        }

        public void setValue(float val) {
            this.value = val;
        }

        public float getValue() {
            return this.value;
        }

        public int getLength() {
            return this.length;
        }

        public void setEnabled(Boolean status) {
            this.enabled = status;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

    }

    public void setAveragePower(AveragePower AveragePower) {
        this.averagePower = averagePower;
    }

    public AveragePower getAveragePower() {
        return averagePower;
    }

    public static class TotalEnergy {
        private float value;
        private String name = "Total Energy";
        private String unit = "kcal";
        private float resolution = 1.0f;
        private int length = BaseUtils.UINT16;
        private boolean enabled;

        public float getResolution() {
            return resolution;
        }

        public String getName() {
            return name;
        }

        public void setValue(float val) {
            this.value = val;
        }

        public float getValue() {
            return this.value;
        }

        public int getLength() {
            return this.length;
        }

        public void setEnabled(Boolean status) {
            this.enabled = status;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

    }

    public void setTotalEnergy(TotalEnergy TotalEnergy) {
        this.totalEnergy = totalEnergy;
    }

    public TotalEnergy getTotalEnergy() {
        return totalEnergy;
    }


    public static class EnergyPerHour {
        private float value;
        private String name = "Total Energy";
        private String unit = "kcal";
        private float resolution = 1.0f;
        private int length = BaseUtils.UINT16;
        private boolean enabled;

        public float getResolution() {
            return resolution;
        }

        public String getName() {
            return name;
        }

        public void setValue(float val) {
            this.value = val;
        }

        public float getValue() {
            return this.value;
        }

        public int getLength() {
            return this.length;
        }

        public void setEnabled(Boolean status) {
            this.enabled = status;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

    }

    public void setEnergyPerHour(EnergyPerHour EnergyPerHour) {
        this.energyPerHour = energyPerHour;
    }

    public EnergyPerHour getEnergyPerHour() {
        return energyPerHour;
    }

    public static class EnergyPerMinute {
        private float value;
        private String name = "Total Energy";
        private String unit = "kcal";
        private float resolution = 1.0f;
        private int length = BaseUtils.UINT8;
        private boolean enabled;

        public float getResolution() {
            return resolution;
        }

        public String getName() {
            return name;
        }

        public void setValue(float val) {
            this.value = val;
        }

        public float getValue() {
            return this.value;
        }

        public int getLength() {
            return this.length;
        }

        public void setEnabled(Boolean status) {
            this.enabled = status;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

    }

    public void setEnergyPerMinute(EnergyPerMinute EnergyPerMinute) {
        this.energyPerMinute = energyPerMinute;
    }

    public EnergyPerMinute getEnergyPerMinute() {
        return energyPerMinute;
    }


    public static class HeartRate {
        private float value;
        private String name = "Heart Rate";
        private String unit = "bpm";
        private float resolution = 1.0f;
        private int length = BaseUtils.UINT8;
        private boolean enabled;

        public float getResolution() {
            return resolution;
        }

        public String getName() {
            return name;
        }

        public void setValue(float val) {
            this.value = val;
        }

        public float getValue() {
            return this.value;
        }

        public int getLength() {
            return this.length;
        }

        public void setEnabled(Boolean status) {
            this.enabled = status;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

    }

    public void setHeartRate(HeartRate heartRate) {
        this.heartRate = heartRate;
    }

    public HeartRate getHeartRate() {
        return heartRate;
    }


    public static class MetabolicEquivalent {
        private float value;
        private String name = "Metabolic Equivalent";
        private String unit = "MET";
        private float resolution = 0.1f;
        private int length = BaseUtils.UINT8;
        private boolean enabled;

        public float getResolution() {
            return resolution;
        }

        public String getName() {
            return name;
        }

        public void setValue(float val) {
            this.value = val;
        }

        public float getValue() {
            return this.value;
        }

        public int getLength() {
            return this.length;
        }

        public void setEnabled(Boolean status) {
            this.enabled = status;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

    }

    public void setMetabolicEquivalent(MetabolicEquivalent metabolicEquivalent) {
        this.metabolicEquivalent = metabolicEquivalent;
    }

    public MetabolicEquivalent getMetabolicEquivalent() {
        return metabolicEquivalent;
    }


    public static class ElapsedTime {
        private float value;
        private String name = "Elapsed Time";
        private String unit = "s";
        private float resolution = 1.0f;
        private int length = BaseUtils.UINT16;
        private boolean enabled;

        public float getResolution() {
            return resolution;
        }

        public String getName() {
            return name;
        }

        public void setValue(float val) {
            this.value = val;
        }

        public float getValue() {
            return this.value;
        }

        public int getLength() {
            return this.length;
        }

        public void setEnabled(Boolean status) {
            this.enabled = status;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

    }

    public void setElapsedTime(ElapsedTime elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public ElapsedTime getElapsedTime() {
        return elapsedTime;
    }

    public static class RemainingTime {
        private float value;
        private String name = "Remaining Time";
        private String unit = "s";
        private float resolution = 1.0f;
        private int length = BaseUtils.UINT16;
        private boolean enabled;

        public float getResolution() {
            return resolution;
        }

        public String getName() {
            return name;
        }

        public void setValue(float val) {
            this.value = val;
        }

        public float getValue() {
            return this.value;
        }

        public int getLength() {
            return this.length;
        }

        public void setEnabled(Boolean status) {
            this.enabled = status;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

    }

    public void setRemainingTime(RemainingTime remainingTime) {
        this.remainingTime = remainingTime;
    }

    public RemainingTime getRemainingTime() {
        return remainingTime;
    }


}
