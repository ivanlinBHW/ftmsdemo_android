package com.example.ftmsDemo.model;


import android.bluetooth.BluetoothGattCharacteristic;

import com.example.ftmsDemo.util.BaseUtils;
import com.example.ftmsDemo.util.Helper;

import java.util.ArrayList;
import java.util.List;

public class DataRecordIndoorBike {
    private byte[] flag = new byte[2];
    public List<byte[]> dataList;
    public List<Boolean> fieldList;
    public static int intOrThrow;
    private BluetoothGattCharacteristic characteristic;
    public static IndoorBikeData data;
    private static boolean isCountDown = false;
    public static boolean startCount;
    public static boolean isPaused = false;


    public DataRecordIndoorBike(BluetoothGattCharacteristic characteristic) {

        /**
        *IndoorBikeData.InstantaneousSpeed;
        *IndoorBikeData.AverageSpeed;
        *IndoorBikeData.InstantaneousCadence;
        *IndoorBikeData.AverageCadence;
        *IndoorBikeData.TotalDistance;
        *IndoorBikeData.InstantaneousPower;
        *IndoorBikeData.AveragePower;
        *IndoorBikeData.TotalEnergy;
        *IndoorBikeData.EnergyPerHour;
        *IndoorBikeData.EnergyPerMinute ;
        *IndoorBikeData.HeartRate;
        *IndoorBikeData.MetabolicEquivalent;
        *IndoorBikeData.ElapsedTime;
        *IndoorBikeData.RemainingTime;
         */

        int intOrThrow = BaseUtils.intOrThrow(characteristic.getIntValue(18, 0));

        Helper.log("====indoor=====intOrThrow===TR============"+intOrThrow);
        Helper.log("0=================================="+((intOrThrow & (1 << 0))==0));
        Helper.log("1=================================="+((intOrThrow & (1 << 1))!=0));
        Helper.log("2=================================="+((intOrThrow & (1 << 2))!=0));
        Helper.log("3=================================="+((intOrThrow & (1 << 3))!=0));
        Helper.log("4=================================="+((intOrThrow & (1 << 4))!=0));
        Helper.log("5=================================="+((intOrThrow & (1 << 5))!=0));
        Helper.log("6=================================="+((intOrThrow & (1 << 6))!=0));
        Helper.log("7=================================="+((intOrThrow & (1 << 7))!=0));
        Helper.log("8=================================="+((intOrThrow & (1 << 8))!=0));
        Helper.log("8=================================="+((intOrThrow & (1 << 8))!=0));
        Helper.log("8=================================="+((intOrThrow & (1 << 8))!=0));
        Helper.log("9=================================="+((intOrThrow & (1 << 9))!=0));
        Helper.log("10=================================="+((intOrThrow & (1 << 10))!=0));
        Helper.log("11=================================="+((intOrThrow & (1 << 11))!=0));
        Helper.log("12=================================="+((intOrThrow & (1 << 12))!=0));
        Helper.log("13=======reservedforfutureuse======="+((intOrThrow & (1 << 13))!=0));

        fieldList = new ArrayList<Boolean>();
        fieldList.add(((intOrThrow & (1 << 0))==0));
        fieldList.add(((intOrThrow & (1 << 1))!=0));
        fieldList.add(((intOrThrow & (1 << 2))!=0));
        fieldList.add(((intOrThrow & (1 << 3))!=0));
        fieldList.add(((intOrThrow & (1 << 4))!=0));
        fieldList.add(((intOrThrow & (1 << 5))!=0));
        fieldList.add(((intOrThrow & (1 << 6))!=0));
        fieldList.add(((intOrThrow & (1 << 7))!=0));
        fieldList.add(((intOrThrow & (1 << 8))!=0));
        fieldList.add(((intOrThrow & (1 << 8))!=0));
        fieldList.add(((intOrThrow & (1 << 8))!=0));
        fieldList.add(((intOrThrow & (1 << 9))!=0));
        fieldList.add(((intOrThrow & (1 << 10))!=0));
        fieldList.add(((intOrThrow & (1 << 11))!=0));
        fieldList.add(((intOrThrow & (1 << 12))!=0));
        fieldList.add(((intOrThrow & (1 << 13))!=0));

        byte[] buffer = characteristic.getValue();
        data  = IndoorBikeData.getInstance();
        int srcPos = 2;

        if(fieldList.get(0)) {
            data.getInstantaneousSpeed().setEnabled(true);
            byte[] val = new byte[data.getInstantaneousSpeed().getLength()];
            System.arraycopy(buffer, srcPos, val, 0, data.getInstantaneousSpeed().getLength());
            srcPos += data.getInstantaneousSpeed().getLength();
            data.getInstantaneousSpeed().setValue(BaseUtils.bytes2int2(val, 0) * data.getInstantaneousSpeed().getResolution());
            Helper.log("=============Val0 getInstantaneousSpeed=================" + data.getInstantaneousSpeed().getValue());
        }else{
            data.getInstantaneousSpeed().setEnabled(false);
        }

        if(fieldList.get(1)) {
            data.getAverageSpeed().setEnabled(true);
            byte[] val = new byte[data.getAverageSpeed().getLength()];
            System.arraycopy(buffer, srcPos, val, 0, data.getAverageSpeed().getLength());
            srcPos += data.getAverageSpeed().getLength();
            data.getAverageSpeed().setValue(BaseUtils.bytes2int2(val, 0) * data.getAverageSpeed().getResolution());
            Helper.log("=============Val1 getAverageSpeed================="+data.getAverageSpeed().getValue());
        }else{
            data.getAverageSpeed().setEnabled(false);
        }

        if(fieldList.get(2)) {
            data.getInstantaneousCadence().setEnabled(true);
            byte[] val = new byte[data.getInstantaneousCadence().getLength()];
            System.arraycopy(buffer, srcPos, val, 0, data.getInstantaneousCadence().getLength());
            srcPos += data.getInstantaneousCadence().getLength();
            data.getInstantaneousCadence().setValue(BaseUtils.bytes2int2(val, 0) * data.getInstantaneousCadence().getResolution());
            Helper.log("=============Val2 getInstantaneousCadence================="+data.getInstantaneousCadence().getValue());
        }else{
            data.getInstantaneousCadence().setEnabled(false);
        }

        if(fieldList.get(3)) {
            data.getAverageCadence().setEnabled(true);
            byte[] val = new byte[data.getAverageCadence().getLength()];
            System.arraycopy(buffer, srcPos, val, 0, data.getAverageCadence().getLength());
            srcPos += data.getAverageCadence().getLength();
            data.getAverageCadence().setValue(BaseUtils.bytes2int2(val, 0) * data.getAverageCadence().getResolution());
            Helper.log("=============Val3 getAverageCadence================="+data.getAverageCadence().getValue());
        }else{
            data.getAverageCadence().setEnabled(false);
        }

        if(fieldList.get(4)) {
            data.getTotalDistance().setEnabled(true);
            byte[] val = new byte[data.getTotalDistance().getLength()];
            System.arraycopy(buffer, srcPos, val, 0, data.getTotalDistance().getLength());
            srcPos += data.getTotalDistance().getLength();
            data.getTotalDistance().setValue(BaseUtils.bytes2int2(val, 0) * data.getTotalDistance().getResolution());
            Helper.log("=============Val4 getTotalDistance================="+data.getTotalDistance().getValue());
        }else{
            data.getTotalDistance().setEnabled(false);
        }

        if(fieldList.get(5)) {
            data.getResistanceLevel().setEnabled(true);
            byte[] val = new byte[data.getResistanceLevel().getLength()];
            System.arraycopy(buffer, srcPos, val, 0, data.getResistanceLevel().getLength());
            srcPos += data.getResistanceLevel().getLength();
            data.getResistanceLevel().setValue(BaseUtils.bytes2int2(val, 0) * data.getResistanceLevel().getResolution());
            Helper.log("=============Val5 getResistanceLevel================="+data.getResistanceLevel().getValue());
        }else{
            data.getResistanceLevel().setEnabled(false);
        }

        if(fieldList.get(6)) {
            data.getInstantaneousPower().setEnabled(true);
            byte[] val = new byte[data.getInstantaneousPower().getLength()];
            System.arraycopy(buffer, srcPos, val, 0, data.getInstantaneousPower().getLength());
            srcPos += data.getInstantaneousPower().getLength();
            data.getInstantaneousPower().setValue(BaseUtils.bytes2int2(val, 0) * data.getInstantaneousPower().getResolution());
            Helper.log("=============Val6 getInstantaneousPower================="+data.getInstantaneousPower().getValue());
        }else{
            data.getInstantaneousPower().setEnabled(false);
        }

        if(fieldList.get(7)) {
            data.getAveragePower().setEnabled(true);
            byte[] val = new byte[data.getAveragePower().getLength()];
            System.arraycopy(buffer, srcPos, val, 0, data.getAveragePower().getLength());
            srcPos += data.getAveragePower().getLength();
            data.getAveragePower().setValue(BaseUtils.bytes2int2(val, 0) * data.getAveragePower().getResolution());
            Helper.log("=============Val7 getAveragePower================="+data.getAveragePower().getValue());
        }else{
            data.getAveragePower().setEnabled(false);
        }

        if(fieldList.get(8)) {
            data.getTotalEnergy().setEnabled(true);
            byte[] val = new byte[data.getTotalEnergy().getLength()];
            System.arraycopy(buffer, srcPos, val, 0, data.getTotalEnergy().getLength());
            srcPos += data.getTotalEnergy().getLength();
            data.getTotalEnergy().setValue(BaseUtils.bytes2int2(val, 0) * data.getTotalEnergy().getResolution());
            Helper.log("=============Val8 getTotalEnergy================="+data.getTotalEnergy().getValue());
        }else{
            data.getTotalEnergy().setEnabled(false);
        }

        if(fieldList.get(9)) {
            data.getEnergyPerHour().setEnabled(true);
            byte[] val = new byte[data.getEnergyPerHour().getLength()];
            System.arraycopy(buffer, srcPos, val, 0, data.getEnergyPerHour().getLength());
            srcPos += data.getEnergyPerHour().getLength();
            data.getEnergyPerHour().setValue(BaseUtils.bytes2int2(val, 0) * data.getEnergyPerHour().getResolution());
            Helper.log("=============Val8 getEnergyPerHour================="+data.getEnergyPerHour().getValue());
        }else{
            data.getEnergyPerHour().setEnabled(false);
        }

        if(fieldList.get(10)) {
            data.getEnergyPerMinute().setEnabled(true);
            byte[] val = new byte[data.getEnergyPerMinute().getLength()];
            System.arraycopy(buffer, srcPos, val, 0, data.getEnergyPerMinute().getLength());
            srcPos += data.getEnergyPerMinute().getLength();
            data.getEnergyPerMinute().setValue(BaseUtils.bytes2int1(val, 0) * data.getEnergyPerMinute().getResolution());
            Helper.log("=============Val8 getEnergyPerMinute================="+data.getEnergyPerMinute().getValue());
        }else{
            data.getEnergyPerMinute().setEnabled(false);
        }

        if(fieldList.get(11)) {
            data.getHeartRate().setEnabled(true);
            byte[] val = new byte[data.getHeartRate().getLength()];
            System.arraycopy(buffer, srcPos, val, 0, data.getHeartRate().getLength());
            srcPos += data.getHeartRate().getLength();
            data.getHeartRate().setValue(BaseUtils.bytes2int1(val, 0) * data.getHeartRate().getResolution());
            Helper.log("=============Val9 getHeartRate================="+data.getHeartRate().getValue());
        }else{
            data.getHeartRate().setEnabled(false);
        }

        if(fieldList.get(12)) {
            data.getMetabolicEquivalent().setEnabled(true);
            byte[] val = new byte[data.getMetabolicEquivalent().getLength()];
            System.arraycopy(buffer, srcPos, val, 0, data.getMetabolicEquivalent().getLength());
            srcPos += data.getMetabolicEquivalent().getLength();
            data.getMetabolicEquivalent().setValue(BaseUtils.bytes2int1(val, 0) * data.getMetabolicEquivalent().getResolution());
            Helper.log("=============Val10 getMetabolicEquivalent================="+data.getMetabolicEquivalent().getValue());
        }else{
            data.getMetabolicEquivalent().setEnabled(false);
        }

        if(startCount && !isPaused)
        if(fieldList.get(13)) {
            data.getElapsedTime().setEnabled(true);
            byte[] val = new byte[data.getElapsedTime().getLength()];
            System.arraycopy(buffer, srcPos, val, 0, data.getElapsedTime().getLength());
            if((BaseUtils.bytes2int2(val, 0))>=1) {
                isCountDown = true;
            }else{
                isCountDown = false;
            }
            if(data.getElapsedTime().getValue()<=(BaseUtils.bytes2int2(val, 0)) && (BaseUtils.bytes2int2(val, 0))>=1){
                data.getElapsedTime().setValue(BaseUtils.bytes2int2(val, 0) * data.getElapsedTime().getResolution());
            }
            Helper.log("=============Val11 getElapsedTime================="+data.getElapsedTime().getValue());
        }else{
            data.getElapsedTime().setEnabled(false);
            if(isCountDown) {
                data.getElapsedTime().setValue(data.getElapsedTime().getValue()+1);
            }else{
                if(startCount) data.getElapsedTime().setValue(1);
            }
        }

        if(fieldList.get(14)) {
            data.getRemainingTime().setEnabled(true);
            byte[] val = new byte[data.getRemainingTime().getLength()];
            System.arraycopy(buffer, srcPos, val, 0, data.getRemainingTime().getLength());
            srcPos += data.getRemainingTime().getLength();
            data.getRemainingTime().setValue(BaseUtils.bytes2int2(val, 0) * data.getRemainingTime().getResolution());
            Helper.log("=============Val12 getRemainingTime =================" + data.getRemainingTime().getValue());
        }else{
            data.getRemainingTime().setEnabled(false);
        }

    }

    /**
     * @return flag
     */
    public byte[] getFlag() {
        return flag;
    }

    /**
     * @return
     */
    public  List<byte[]> getDataList() {
        return dataList;
    }
}

