package com.example.ftmsDemo.util;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.example.ftmsDemo.model.DataRecordIndoorBike;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class ConnectManager {

    public static int connectStatus;

    //Service
    public static UUID FITNESS_MACHINE_SERVICE = UUID.fromString("00001826-0000-1000-8000-00805F9B34FB");

    //Service
    public static UUID DEVICE_INFORMATION_SERVICE = UUID.fromString("0000180a-0000-1000-8000-00805F9B34FB");

    //characteristics
    //fitness_machine_feature
    public static final UUID FINESS_MACHINE_FEATURE = UUID.fromString("00002acc-0000-1000-8000-00805F9B34FB");
    //indoor_bike_data
    public static final UUID INDOOR_BIKE_DATA = UUID.fromString("00002ad2-0000-1000-8000-00805F9B34FB");
    //crossTrainer_data
    public static final UUID CROSS_TRAINER_DATA = UUID.fromString("00002ace-0000-1000-8000-00805F9B34FB");
    //treadmill_data
    public static final UUID TREADMILL_DATA = UUID.fromString("00002acd-0000-1000-8000-00805F9B34FB");
    //support_resistance_level_range
    public static final UUID SUPPORT_RESISTANCE_LEVEL_RANGE = UUID.fromString("00002ad6-0000-1000-8000-00805F9B34FB");
    //fitness_machine_control_point
    public static final UUID FITNESS_MACHINE_CONTROL_POINT = UUID.fromString("00002ad9-0000-1000-8000-00805F9B34FB");
    //fitness_machine_status
    public static final UUID FITNESS_MACHINE_STATUS = UUID.fromString("00002ada-0000-1000-8000-00805F9B34FB");
    public static final UUID TRAINING_STATUS = UUID.fromString("00002ad3-0000-1000-8000-00805f9b34fb");

    public static final UUID UUID_BLE_SERVICE_HEART_RATE = UUID.fromString("0000180d-0000-1000-8000-00805f9b34fb");
    public static final UUID UUID_BLE_CHARACTERISTIC_HEART_RATE = UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb");

    public static final UUID DESCR = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    public static final UUID SUPPORTED_SPEED_RANGE = UUID.fromString("00002ad4-0000-1000-8000-00805f9b34fb");

    public static final UUID SUPPORTED_INCLINATION_RANGE = UUID.fromString("00002ad5-0000-1000-8000-00805f9b34fb");

    public static final UUID SUPPORTED_RESISTANCE_LEVEL_RANGE = UUID.fromString("00002ad6-0000-1000-8000-00805f9b34fb");

    public static final UUID SUPPORTED_HEART_RATE_RANGE = UUID.fromString("00002ad7-0000-1000-8000-00805f9b34fb");

    public static final UUID SUPPORTED_POWER_RANGE = UUID.fromString("00002ad8-0000-1000-8000-00805f9b34fb");

    public static final UUID MODEL_NUMBER = UUID.fromString("00002a24-0000-1000-8000-00805F9B34FB");

    public static float mileUnitConvert = 1.609344f;

    public static float inchUnitConvert = 2.54f;

    public static float lbsUnitConvert = 2.2046f;

    public static float kgUnitConvert = 0.45359f;

    private List<BluetoothGattCharacteristic> characteristicSupportedArray;

    private ScanResult scanResult;

    private static BluetoothGatt gatt;

    private String TAG = "ConnectManager==";

    private Context context;

    public ConnectManagerListener listener;
    public static BleDevice currentDevice;

    private List<WriteCommand> commandList = new ArrayList<WriteCommand>();
    private boolean runCmd;

    private static class SingletonHolder {
        private static final ConnectManager INSTANCE = new ConnectManager();
    }

    public ConnectManager() {
    }

    public static final ConnectManager getInstance() {
        return SingletonHolder.INSTANCE;
    }


    public void setScanResult(ScanResult d) {
        this.scanResult = d;
    }

    public ScanResult getScanResult() {
        return this.scanResult;
    }


    public void startConnect(Context context, ScanResult scanResult, BleDevice device, final ConnectManagerListener listener) {
        this.listener = listener;
        this.context = context;
        this.scanResult = scanResult;
        characteristicSupportedArray = new ArrayList<BluetoothGattCharacteristic>();
        if (gatt != null) {
            disconnectAll();
        }
        gatt = this.scanResult.getDevice().connectGatt(context, false, new BluetoothGattCallback() {
                    @Override
                    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                        super.onConnectionStateChange(gatt, status, newState);
                        //int STATE_DISCONNECTED = 0; int STATE_CONNECTING = 1; int STATE_CONNECTED = 2; int STATE_DISCONNECTING = 3; status 133 GATT_ERROR; status 22 GATT_CONN_TERMINATE_LOCAL_HOST; status 8 GATT_CONN_TIMEOUT;
                        connectStatus = newState;
                        if (newState == BluetoothGatt.STATE_CONNECTED) {
                            currentDevice = device;
                            currentDevice.setConnectType(BleDevice.ConnectType.CONNECT);
                            //currentDevice.setScanResult(scanResult);
                            listener.onFinishedConnect(gatt, connectStatus);
                        } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
                            if(currentDevice!=null)
                                currentDevice.setConnectType(BleDevice.ConnectType.DISCONNECT);
                            currentDevice = null;
                            listener.onFinishedConnect(gatt, connectStatus);
                            gatt.close();
                        } else {
                            if(currentDevice!=null)
                                currentDevice.setConnectType(BleDevice.ConnectType.DISCONNECT);
                            currentDevice = null;
                            listener.onFinishedConnect(gatt, connectStatus);

                        }

                        ConnectManager.getInstance().listener.onConnectionStateChange(gatt, status, newState);
                        gatt.discoverServices();
                    }

                    @Override
                    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                        super.onServicesDiscovered(gatt, status);
                        logthis("OnserviceDiscovered");
                        List<BluetoothGattService> services = gatt.getServices();
                        BluetoothGattCharacteristic characteristic = null;
                        for (BluetoothGattService service : services) {

                            logthis("UUID IS " + service.getUuid().toString());
                            for (BluetoothGattCharacteristic serviceCharacteristic : service.getCharacteristics()) {

                                characteristic = serviceCharacteristic;
                                // Check characteristic property
                                final int properties = characteristic.getProperties();
                                if ((properties & BluetoothGattCharacteristic.PROPERTY_NOTIFY) == 0) {
                                    logthis("no Notify==== ");
                                } else {
                                    logthis("hasNotify==== ");
                                }

                                logthis("char name is==== " + characteristic.getUuid().toString());
                                logthis("char value is==== " + characteristic.getValue());


                                if (characteristic.getUuid().equals(SUPPORTED_SPEED_RANGE)) {
                                    characteristicSupportedArray.add(characteristic);
                                }

                                if (characteristic.getUuid().equals(SUPPORTED_INCLINATION_RANGE)) {
                                    characteristicSupportedArray.add(characteristic);
                                }

                                if (characteristic.getUuid().equals(SUPPORTED_RESISTANCE_LEVEL_RANGE)) {
                                    characteristicSupportedArray.add(characteristic);
                                }

                                if (characteristic.getUuid().equals(SUPPORTED_HEART_RATE_RANGE)) {
                                    characteristicSupportedArray.add(characteristic);
                                }

                                if (characteristic.getUuid().equals(SUPPORTED_POWER_RANGE)) {
                                    characteristicSupportedArray.add(characteristic);
                                }

                                if (characteristic.getUuid().equals(ConnectManager.FINESS_MACHINE_FEATURE)) {
                                    characteristicSupportedArray.add(characteristic);
                                }

                                if (characteristic.getUuid().equals(ConnectManager.MODEL_NUMBER)) {
                                    characteristicSupportedArray.add(characteristic);
                                }

                            }

                            if (service.getUuid().equals(FITNESS_MACHINE_SERVICE)) {
                                boolean successfullyRead = gatt.readCharacteristic(characteristicSupportedArray.get(0));
                                characteristicSupportedArray.remove(0);
                            }
                        }

                        WriteCommand writeCommand = new WriteCommand();
                        writeCommand = new WriteCommand();
                        writeCommand.op_code = new byte[]{0x00};
                        boolean statusFirst = writeCharacteristic(writeCommand);

                        if (connectStatus == 2) {

                            if (ConnectManager.currentDevice == null) return;

                            writeCommand = new WriteCommand();
                            writeCommand.op_code = new byte[]{0x07};
                            boolean isSuccess = writeCharacteristic(writeCommand);
                        }
                        listener.onServicesDiscovered(gatt, status);
                    }

                    @Override
                    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                        super.onCharacteristicRead(gatt, characteristic, status);

                        if (status == BluetoothGatt.GATT_SUCCESS) {
                            byte[] characteristicValue = characteristic.getValue();
                            String modelName = new String(characteristic.getValue());
                            logthis("char is " + modelName);


                            if (characteristicSupportedArray.size() > 0) {
                                boolean successfullyRead = gatt.readCharacteristic(characteristicSupportedArray.get(0));
                                characteristicSupportedArray.remove(0);
                                logthis("Read characteristic " + successfullyRead);
                            } else {
                                //notify
                                setCharacteristicNotification(ConnectManager.FITNESS_MACHINE_CONTROL_POINT, true);
                                setCharacteristicNotification(ConnectManager.FITNESS_MACHINE_STATUS, false);
                                setCharacteristicNotification(ConnectManager.TRAINING_STATUS, false);
                                setCharacteristicNotification(ConnectManager.INDOOR_BIKE_DATA, false);
                            }

                        } else if (status == BluetoothGatt.GATT_READ_NOT_PERMITTED) {
                            logthis("No permitted to read a characteristic");
                        } else if (status == BluetoothGatt.GATT_FAILURE) {
                            logthis("failed to read a characteristic");
                        }
                        listener.onCharacteristicRead(gatt, characteristic, status);
                    }

                    @Override
                    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                        byte[] data = characteristic.getValue();
                        if (data != null && data.length > 0) {
                            final StringBuilder stringBuilder = new StringBuilder(data.length);
                            for (byte byteChar : data)
                                stringBuilder.append(String.format("%02X ", byteChar));
                        } else {

                        }
                        super.onCharacteristicWrite(gatt, characteristic, status);
                        listener.onCharacteristicWrite(gatt, characteristic, status);
                    }

                    @Override
                    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                        Helper.log("onCharacteristicChanged");
                        byte[] data = characteristic.getValue();

                        String dataNotify = BaseUtils.bytesToHexString(characteristic.getValue());
                        Helper.log("-=-=-=-=-=-=-=-=-=-=-=-=" + dataNotify);
                        Helper.log("-=-=-=-=-=UDID-=-=-=-=-=-=-=" + characteristic.getUuid());


                        if (data != null && data.length > 0) {
                            final StringBuilder stringBuilder = new StringBuilder(data.length);
                            for (byte byteChar : data)
                                stringBuilder.append(String.format("%02X ", byteChar));
                            Helper.log("Data:" + stringBuilder.toString());
                            Helper.log("Data length:" + data.length);
                            //Helper.log("接收到的getDescriptors:" +BaseUtils.bytes2ToInt(characteristic.getDescriptors().get(0).getValue(),0));
                        }

                        if (characteristic.getUuid().equals(ConnectManager.FITNESS_MACHINE_CONTROL_POINT)) {
                            if (String.format("%02X ", data[0]).compareTo("80") == 1) {
                                if (data[1] == 0x08) { //Stop or Pause
                                    switch (data[2]) {
                                        case 0x00:
                                            //Reserved for Future Use
                                            break;
                                        case 0x01:
                                            //Success
                                            break;
                                        case 0x02:
                                            //Op Code not supported
                                            break;
                                        case 0x03:
                                            //Invalid Parameter
                                            break;
                                        case 0x04:
                                            //Operation Failed
                                            break;
                                        case 0x05:
                                            //Control Not Permitted
                                            if (!runCmd) {
                                                WriteCommand writeCommand = new WriteCommand();
                                                writeCommand.op_code = new byte[]{data[1]};
                                                commandList.add(writeCommand);

                                                writeCommand = new WriteCommand();
                                                writeCommand.op_code = new byte[]{0x00};
                                                boolean status = writeCharacteristic(writeCommand);
                                                runCmd = true;
                                            }
                                            break;
                                        case 0x06:
                                            //Reserved for Future Use
                                            break;
                                    }

                                } else if (data[1] == 0x07) {
                                    switch (data[2]) {
                                        case 0x00:
                                            //Reserved for Future Use
                                            break;
                                        case 0x01:
                                            //Success
                                            Helper.log("xx00 startSuccess true");
                                            break;
                                        case 0x02:
                                            //Op Code not supported
                                            break;
                                        case 0x03:
                                            //Invalid Parameter
                                            break;
                                        case 0x04:
                                            Helper.log("Operation Failed!!!");
                                            ((Activity) context).runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Handler handler = new Handler(Looper.getMainLooper());
                                                    handler.postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            disconnectAll();
                                                            ((Activity) context).finish();
                                                        }
                                                    }, 100);
                                                }
                                            });
                                            //Operation Failed
                                            break;
                                        case 0x05:
                                            //Control Not Permitted
                                            if (!runCmd) {
                                                WriteCommand writeCommand = new WriteCommand();
                                                writeCommand.op_code = new byte[]{0x00};
                                                commandList.add(writeCommand);

                                                writeCommand = new WriteCommand();
                                                writeCommand.op_code = new byte[]{0x07};
                                                commandList.add(writeCommand);

                                                //runCmd = true;
                                            }
                                            break;
                                        case 0x06:
                                            //Reserved for Future Use
                                            break;

                                    }
                                } else if (data[1] == 0x00) {
                                    switch (data[2]) {
                                        case 0x00:
                                            //Reserved for Future Use
                                            break;
                                        case 0x01:
                                            //Success
                                            WriteCommand writeCommand = new WriteCommand();
                                            writeCommand.op_code = new byte[]{0x07};
                                            boolean status = writeCharacteristic(writeCommand);
                                            break;
                                        case 0x02:
                                            //Op Code not supported
                                            break;
                                        case 0x03:
                                            //Invalid Parameter
                                            break;
                                        case 0x04:
                                            Helper.log("Operation Failed!!!");
                                            ((Activity) context).runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Handler handler = new Handler(Looper.getMainLooper());
                                                    handler.postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            disconnectAll();
                                                            ((Activity) context).finish();
                                                        }
                                                    }, 100);
                                                }
                                            });
                                            //Operation Failed
                                            break;
                                        case 0x05:
                                            //Control Not Permitted
                                            break;
                                        case 0x06:
                                            //Reserved for Future Use
                                            break;

                                    }

                                }
                            }

                        } else if (characteristic.getUuid().equals(ConnectManager.FITNESS_MACHINE_STATUS)) {
                            int opcode = data[0];
                            switch (opcode) {
                                case 0x01:
                                    Helper.log("Fitness Machine Status | Op Code: Reset");
                                    ((Activity) context).finish();
                                    break;
                                case 0x02:
                                    Helper.log("Fitness Machine Status | Op Code: Fitness Machine Stopped or Paused by the User");
                                    int parameter = data[1];
                                    switch (parameter) {
                                        case 0x01:
                                            Helper.log(" parameter = Stop");
                                            ((Activity) context).finish();
                                            break;
                                        case 0x02:
                                            Helper.log(" parameter = Pause");
                                            break;
                                    }
                                    break;
                                case 0x03:
                                    Helper.log("Fitness Machine Status | Op Code: Fitness Machine Stopped by Safety Key");
                                    ((Activity) context).finish();
                                    break;
                                case 0x04:
                                    Helper.log("Fitness Machine Status | Op Code: Fitness Machine Started or Resumed by the User");
                                    break;
                                case 0x05:
                                    Helper.log("Fitness Machine Status | Op Code: Target Speed Changed");
                                    break;
                                case 0x06:
                                    Helper.log("Fitness Machine Status | Op Code: Target Incline Changed");
                                    break;
                                case 0x07:
                                    Helper.log("Fitness Machine Status | Op Code: Target Resistance Level Changed");
                                    break;
                                case 0x08:
                                    Helper.log("Fitness Machine Status | Op Code: Target Power Changed");
                                    break;
                                case 0x09:
                                    Helper.log("Fitness Machine Status | Op Code: Target Heart Rate Changed");
                                    break;
                                case 0x0A:
                                    Helper.log("Fitness Machine Status | Op Code: Targeted Expended Energy Changed");
                                    break;
                                case 0x0B:
                                    Helper.log("Fitness Machine Status | Op Code: Targeted Number of Steps Changed");
                                    break;
                                case 0x0C:
                                    Helper.log("Fitness Machine Status | Op Code: Targeted Number of Strides Changed");
                                    break;
                                case 0x0D:
                                    Helper.log("Fitness Machine Status | Op Code: Targeted Distance Changed");
                                    break;
                                case 0x0E:
                                    Helper.log("Fitness Machine Status | Op Code: Targeted Training Time Changed");
                                    break;
                                case 0x0F:
                                    Helper.log("Fitness Machine Status | Op Code: Targeted Time in Two Heart Rate Zones Changed");
                                    break;
                                case 0x10:
                                    Helper.log("Fitness Machine Status | Op Code: Targeted Time in Three Heart Rate Zones Changed");
                                    break;
                                case 0x11:
                                    Helper.log("Fitness Machine Status | Op Code: Targeted Time in Five Heart Rate Zones Changed");
                                    break;
                                case 0x12:
                                    Helper.log("Fitness Machine Status | Op Code: Indoor Bike Simulation Parameters Changed");
                                    break;
                                case 0x13:
                                    Helper.log("Fitness Machine Status | Op Code: Wheel Circumference Changed");
                                    break;
                                case 0x14:
                                    Helper.log("Fitness Machine Status | Op Code: Spin Down Status");
                                    break;
                                case 0x15:
                                    Helper.log("Fitness Machine Status | Op Code: Targeted Cadence Changed");
                                    break;
                                case 0x16:
                                    Helper.log("Fitness Machine Status | Op Code: Reserved for Future Use"); //0x16-0xFE
                                    break;
                                case 0xFF:
                                    Helper.log("Fitness Machine Status | Op Code: Control Permission Lost");
                                    break;
                            }
                        } else if (characteristic.getUuid().equals(ConnectManager.TRAINING_STATUS)) {
                            switch (data[1]) {
                                case 0x00:
                                    //Ohter
                                    break;
                                case 0x01:
                                    //Idel
                                    break;
                                case 0x02:
                                    //Warming Up
                                    break;
                                case 0x03:
                                    //Low Intensity interval
                                    break;
                                case 0x04:
                                    //High Intensity interval
                                    break;
                                case 0x05:
                                    //Recovery interval
                                    break;
                                case 0x06:
                                    //Isometric
                                    break;
                                case 0x07:
                                    //Heart Rate Control
                                    break;
                                case 0x08:
                                    //Fitness Test
                                    break;
                                case 0x09:
                                    //Speed Outside of Control Region - Low (increase speed to return to controllable region)
                                    break;
                                case 0x0a:
                                    //Speed Outside of Control Region - High (decrease speed to return to controllable region)
                                    break;
                                case 0x0b:
                                    //Cool Down
                                    break;
                                case 0x0c:
                                    //Watt Control
                                    break;
                                case 0x0d:
                                    DataRecordIndoorBike.startCount = true;
                                    //Manual Mode(Quick Start)
                                    break;
                                case 0x0e:
                                    DataRecordIndoorBike.startCount = false;
                                    break;
                                case 0x0f:
                                    //Post-Workout
                                    break;
                                case 0x10:
                                    //Reserved for Future Use
                                    break;
                            }


                        }
                        listener.onCharacteristicChanged(gatt, characteristic);

                    }

                    @Override
                    public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                        super.onDescriptorRead(gatt, descriptor, status);
                        logthis("onDescriptorRead");
                        if (status == BluetoothGatt.GATT_SUCCESS) {
                            byte[] descriptorValue = descriptor.getValue();
                            logthis("Descriptor: " + descriptorValue.toString());
                        } else if (status == BluetoothGatt.GATT_READ_NOT_PERMITTED) {
                            logthis("No permitted to read a descriptor");
                        } else if (status == BluetoothGatt.GATT_FAILURE) {
                            logthis("failed to read a descriptor");
                        }
                        listener.onDescriptorRead(gatt, descriptor, status);

                    }
                }
        );
    }


    //set nofication or indication enable
    public boolean setCharacteristicNotification(UUID characteristic_uuid, boolean isIndication) {
        boolean result = false;
        if (gatt == null) {
            return false;
        }
        BluetoothGattService service = gatt.getService(FITNESS_MACHINE_SERVICE);
        if (service == null) {
            return false;
        }
        BluetoothGattCharacteristic characteristic = service.getCharacteristic(characteristic_uuid);
        if (characteristic == null) {
            return false;
        }
        gatt.setCharacteristicNotification(characteristic, true);

        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(DESCR);
        if (descriptor == null) {
            return false;
        }
        if (isIndication) {
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
        } else {
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        }
        result = gatt.writeDescriptor(descriptor);
        if (result) {
            Helper.log("setCharacteristicNotification success");
        }
        return result;
    }


    public boolean writeCharacteristic(WriteCommand writeCommand) {
        boolean status = false;
        try {
            if (gatt == null) {
                return false;
            }
            BluetoothGattService Service = gatt.getService(FITNESS_MACHINE_SERVICE);
            if (Service == null) {
                return false;
            }

            BluetoothGattCharacteristic charac = Service
                    .getCharacteristic(FITNESS_MACHINE_CONTROL_POINT);
            if (charac == null) {
                Log.e(TAG, "char not found!");
                return false;
            }
            if (writeCommand.value != null) {
                charac.setValue(new byte[writeCommand.length + 1]);
                charac.setValue(writeCommand.op_code[0], BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                charac.setValue(Integer.valueOf(writeCommand.value), BluetoothGattCharacteristic.FORMAT_UINT16, 1);
            } else {
                charac.setValue(writeCommand.op_code);
            }
            charac.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);

            status = gatt.writeCharacteristic(charac);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public void disconnectAll() {
        if (gatt != null) {
            currentDevice = null;
            gatt.disconnect();
            gatt.close();
            gatt = null;
        }
    }

    public void logthis(final String msg) {
        if (context instanceof Activity) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //logger.append(msg + "\n");
                    Log.d(TAG, msg);
                }
            });
        }
    }

    public interface ConnectManagerListener {
        void onConnectionStateChange(BluetoothGatt gatt, int status, int newState);

        void onServicesDiscovered(BluetoothGatt gatt, int status);

        void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status);

        void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status);

        void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic);

        void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status);

        void onFinishedConnect(BluetoothGatt gatt, int newState);

    }


}


