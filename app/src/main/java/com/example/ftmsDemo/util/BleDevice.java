package com.example.ftmsDemo.util;

import android.bluetooth.le.ScanResult;

public class BleDevice {

    private ScanResult scanResult;
    private int id;
    private boolean visible;
    private String deviceAddress;
    private ConnectType connectType;
    private BleManager.Type deviceTypeByFTMS;
    private String imgUrl;
    private int imgResource;
    private String displayName;
    private String modelNumberPic;

    public enum ConnectType {
        CONNECT, DISCONNECT, NOT_FOUND
    }

    public BleDevice(ScanResult scanResult, String deviceName) {
        this.scanResult = scanResult;
        this.displayName = getDisplayName(scanResult.getDevice().getName());
        this.deviceAddress = scanResult.getDevice().getAddress();
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ScanResult getScanResult() {
        return scanResult;
    }

    public void setScanResult(ScanResult scanResult) {
        this.scanResult = scanResult;
    }

    public ConnectType getConnectType() {
        return connectType;
    }

    public void setConnectType(ConnectType connectType) {
        this.connectType = connectType;
    }

    public int getImgResource() {
        return imgResource;
    }

    public String getImgURL() {
        return imgUrl;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getDeviceAddress() {
        return deviceAddress;
    }

    public String getModelNumberPic() {
        return modelNumberPic;
    }

    public void setModelNumberPic(String modelNumberPic) {
        this.modelNumberPic = modelNumberPic;
    }

    public BleManager.Type getDeviceTypeFtms() {
        return deviceTypeByFTMS;
    }

    public void setDeviceTypeFtms(BleManager.Type deviceTypeByFTMS) {
        this.deviceTypeByFTMS = deviceTypeByFTMS;

    }

    private static String getDisplayName(String deviceName) {
        String[] startKeys = {"JFIC", "JFTM", "JFEP", "JFBK"};
        if(deviceName!=null)
        for (String startKey : startKeys) {
            if (deviceName.startsWith(startKey)) {
                return deviceName.replace(startKey, "");
            }
        }
        return deviceName;
    }


}

