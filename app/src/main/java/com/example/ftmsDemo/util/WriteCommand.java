package com.example.ftmsDemo.util;

public class WriteCommand {
    public byte[] op_code;
    public String value;
    public int length;
    public float resolution;
}
