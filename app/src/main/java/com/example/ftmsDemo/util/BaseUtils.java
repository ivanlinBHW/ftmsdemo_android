package com.example.ftmsDemo.util;

public class BaseUtils {

    public static final int UINT8 = 1;
    public static final int UINT16 = 2;
    public static final int UINT24 = 3;
    public static final int UINT32 = 4;
    public static final int SINT8 = -1;
    public static final int SINT16 = -2;
    public static final int SINT24 = -3;
    public static final int SINT32 = -4;


    public static int intOrThrow(Integer num) {
        if (num != null) {
            return num.intValue();
        }
        throw new NullPointerException();
    }

    public static int bytes2int1(byte[] src, int offset) {
        int value;
        value = (int) ((src[offset] & 0xFF));
        return value;
    }

    public static int bytes2int2(byte[] src, int offset) {
        int value;
        value = (int) (((src[offset + 1] & 0xFF) << 8)
                | (src[offset ] & 0xFF));
        return value;
    }

    public static int bytes2int3(byte[] src, int offset) {
        int value;
        value = (int) (((src[offset + 2] & 0xFF) << 16)
                | ((src[offset + 1] & 0xFF) << 8)
                | (src[offset] & 0xFF));
        return value;
    }

    public static int bytes2int4(byte[] src, int offset) {
        int value;
        value = (int) (((src[offset + 3] & 0xFF) << 24)
                | ((src[offset + 2] & 0xFF) << 16)
                | ((src[offset + 1] & 0xFF) << 8)
                | (src[offset] & 0xFF));
        return value;
    }

    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

}