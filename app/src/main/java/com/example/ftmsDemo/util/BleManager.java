package com.example.ftmsDemo.util;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.ParcelUuid;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.ftmsDemo.adapter.BleDeviceAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class BleManager {

    public static BluetoothAdapter bluetoothManagerAdapter;
    public static BleDeviceAdapter bleDeviceAdapter;
    private ScanCallback mScanCallback;
    private static BluetoothLeScanner mScanner;
    private Context context;
    public static final int REQUEST_ACCESS_COURSE_LOCATION = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    public static Map<String, BleDevice> bleAddedDeviceListMap = new HashMap<String, BleDevice>();
    public static Map<String, BleDevice> bleAddedDeviceListHeartRateMap = new HashMap<String, BleDevice>();
    public static List<BleDevice> bleScanDeviceListData = new ArrayList<BleDevice>();
    private ScanResultListener listener;
    private FilterType filterType;
    private boolean isFoundDevice;

    public static enum FilterType{
        SERVICE_HEART_RATE, FITNESS_MACHINE_SERVICE
    }

    private static class SingletonHolder {
        private static final BleManager INSTANCE = new BleManager();
    }

    public BleManager() {
    }

    public static final BleManager getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void init(Context context, BleDeviceAdapter _bleDeviceAdapter, ScanResultListener listener, FilterType type) {
        this.context = context;
        filterType = type;
        if(_bleDeviceAdapter!=null) this.bleDeviceAdapter = _bleDeviceAdapter;
        this.listener = listener;
        checkpermissions();
    }


    //This code will check to see if there is a bluetooth device and
    //turn it on if is it turned off.
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void startbt() {
        bluetoothManagerAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothManagerAdapter == null) {
            // Device does not support Bluetooth
            Log.d("log","This device does not support bluetooth");
            return;
        }
        //make sure bluetooth is enabled.
        if (!bluetoothManagerAdapter.isEnabled()) {
            Log.d("log","There is bluetooth, but turned off");
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            if (context instanceof Activity) {
                ((Activity) context).startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            } else {
                Log.d("log","mContext should be an instanceof Activity.");
            }
        } else {
            Log.d("log","The bluetooth is ready to use.");
            blueToothScan();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void blueToothScan() {
        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager == null) {
            //Handle this issue. Report to the user that the device does not support BLE
            Log.d("log","no bluetooth, shouldn't be here");
        } else {
            bluetoothManagerAdapter = bluetoothManager.getAdapter();
        }
        startScanning();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void startScanning() {
        Log.d("log","startScanning========");

        isFoundDevice = false;
        bleScanDeviceListData.clear();
        if(bleDeviceAdapter!=null) bleDeviceAdapter.notifyDataSetChanged();

        if(mScanner == null )
        mScanner = bluetoothManagerAdapter.getBluetoothLeScanner();

        ScanSettings scanSettings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .build();
        List<ScanFilter> scanFilters = Arrays.asList(
                new ScanFilter.Builder()

                        //.setDeviceName("JFTMOmega Z")
                        .build());
        if (mScanCallback == null) {
            mScanCallback = new ScanCallback();
        }
        mScanner.startScan(scanFilters, scanSettings, mScanCallback);
    }

    public void stopScanning() {
        if (mScanner != null) {
            Log.d("log","Stop scan!");
            mScanner.stopScan(mScanCallback);
        }
    }

    private  BroadcastReceiver bondChangedReceived = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if(action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)){
                Log.d("bond", "Changed bond state");
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device.getBondState() == BluetoothDevice.BOND_BONDING) {
                    Log.d("bond", "if BOND_BONDING");
                }
                if (device.getBondState() == BluetoothDevice.BOND_NONE) {
                    Log.d("bond", "if BOND_NONE");
                }
                if (device.getBondState() == BluetoothDevice.ERROR) {
                    Log.d("bond", "if ERROR");
                }
                if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
                    Log.d("bond", "if BOND");
                }
            }
        }
    };
    private BroadcastReceiver discoveryFinishedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if(action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)){
                Log.d("log", "Discovery finished");
                //Toast.makeText(getApplicationContext(), R.string.discovery_finished, Toast.LENGTH_LONG).show();
            }
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void checkpermissions() {
        if ((ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            Log.d("log","asking for permissions");

            if (context instanceof Activity) {
                ActivityCompat.requestPermissions(((Activity) context), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_ACCESS_COURSE_LOCATION);
                Log.d("log","We don't have permission to course location");
            }
        } else {
            Log.d("log","We have permission to course location");
            startbt();
        }


    }


    public class ScanCallback extends android.bluetooth.le.ScanCallback {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.d("log","Error====="+errorCode);

        }

        @Override
        public void onScanResult(int callbackType, final ScanResult result) {
            BluetoothDevice device = bluetoothManagerAdapter.getRemoteDevice(result.getDevice().getAddress());
            Log.d("log", "scan result" + result.toString());
            boolean contains = false;
            Type currentMachineType = getMachineType(result);
            //reSetData for Rssi
            for (int i = 0; i < bleScanDeviceListData.size(); i++) {
                if(bleScanDeviceListData.get(i).getScanResult()==null) return;
                if (bleScanDeviceListData.get(i).getScanResult().getDevice().equals(device)) {
                    if (context instanceof Activity) {
                        final int finalI = i;
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                bleScanDeviceListData.get(finalI).setScanResult(result);
                                bleScanDeviceListData.get(finalI).setDeviceTypeFtms(currentMachineType);
                                if(bleDeviceAdapter!=null) bleDeviceAdapter.notifyDataSetChanged();
                            }

                        });
                    }

                    contains = true;
                }
            }
            if (!contains || bleScanDeviceListData.size() == 0) {
                List<ParcelUuid> uuidList = result.getScanRecord().getServiceUuids();

                if (uuidList != null){

                    for (int i = 0; i < uuidList.size(); i++) {

                        if(filterType == FilterType.FITNESS_MACHINE_SERVICE && result.getScanRecord().getServiceUuids().get(i).toString().compareTo(ConnectManager.UUID_BLE_SERVICE_HEART_RATE.toString()) == 0 ) return;
                        if(filterType == FilterType.SERVICE_HEART_RATE && result.getScanRecord().getServiceUuids().get(i).toString().compareTo(ConnectManager.FITNESS_MACHINE_SERVICE.toString()) == 0 ) return;

                        if (result.getScanRecord().getServiceUuids().get(i).toString().compareTo(ConnectManager.FITNESS_MACHINE_SERVICE.toString()) == 0) {

                            final BleDevice scanResult = new BleDevice(result, result.getDevice().getName());
                            scanResult.setDeviceTypeFtms(currentMachineType);
                            //ReAdd Result When Close App
                            if(bleAddedDeviceListMap!=null)
                                if(bleAddedDeviceListMap.get(scanResult.getDeviceAddress())!=null){
                                    bleAddedDeviceListMap.get(scanResult.getDeviceAddress()).setScanResult(result);
                                    bleAddedDeviceListMap.get(scanResult.getDeviceAddress()).setDeviceTypeFtms(currentMachineType);
                                }


                            if(bleAddedDeviceListHeartRateMap!=null)
                                if(bleAddedDeviceListHeartRateMap.get(scanResult.getDeviceAddress())!=null){
                                    bleAddedDeviceListHeartRateMap.get(scanResult.getDeviceAddress()).setScanResult(result);
                                }

                            if (context instanceof Activity) {
                                ((Activity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(filterType == FilterType.FITNESS_MACHINE_SERVICE && bleAddedDeviceListMap.get(scanResult.getDeviceAddress())!=null) return;
                                        if(filterType == FilterType.SERVICE_HEART_RATE && bleAddedDeviceListHeartRateMap.get(scanResult.getDeviceAddress())!=null) return;
                                        bleScanDeviceListData.add(scanResult);

                                        listener.onScanResultResponse(result);
                                        if(bleDeviceAdapter!=null){
                                            bleDeviceAdapter.setData(bleScanDeviceListData);
                                            bleDeviceAdapter.notifyDataSetChanged();
                                        }

                                    }

                                });
                            }
                            isFoundDevice = true;
                        }
                    }
                        listener.onScanResultResponse(result);
                }

            }
             listener.onScanResultResponse(result);


        }
    }

    private Type getMachineType(ScanResult result){

        byte[] bytes = result.getScanRecord().getServiceData(ParcelUuid.fromString(ConnectManager.FITNESS_MACHINE_SERVICE.toString()));

        Helper.log("getServiceData====="+BaseUtils.bytesToHexString(bytes));
        Helper.log("getServiceData=====Udid"+result.getDevice().getAddress());


        Type currentMachineTypeFtms = null;

        if(bytes!=null){
            Helper.log("getServiceData=====size"+bytes.length);
            if(bytes.length!=3) {
                currentMachineTypeFtms = Type.Treadmill;
                return currentMachineTypeFtms;
            }
            int intOrThrow = 0;

            intOrThrow = BaseUtils.bytes2int2(bytes,1);

            Helper.log("=========intOrintOrThrowThrow===MachineType============"+intOrThrow+"uuid"+result.getDevice().getAddress());
            Helper.log("0=================================="+((intOrThrow & (1 << 0))!=0));
            Helper.log("1=================================="+((intOrThrow & (1 << 1))!=0));
            Helper.log("2=================================="+((intOrThrow & (1 << 2))!=0));
            Helper.log("3=================================="+((intOrThrow & (1 << 3))!=0));
            Helper.log("4=================================="+((intOrThrow & (1 << 4))!=0));
            Helper.log("5=================================="+((intOrThrow & (1 << 5))!=0));

            ArrayList<Boolean> machineTypeArray = new ArrayList<Boolean>();
            machineTypeArray.add((intOrThrow & (1 << 0))!=0);
            machineTypeArray.add((intOrThrow & (1 << 1))!=0);
            machineTypeArray.add((intOrThrow & (1 << 2))!=0);
            machineTypeArray.add((intOrThrow & (1 << 3))!=0);
            machineTypeArray.add((intOrThrow & (1 << 4))!=0);
            machineTypeArray.add((intOrThrow & (1 << 5))!=0);

            if(machineTypeArray.get(0)) {
                currentMachineTypeFtms = Type.Treadmill;
            }else if(machineTypeArray.get(1)){
                currentMachineTypeFtms = Type.CrossTrainer;
            }else if(machineTypeArray.get(2)){
                currentMachineTypeFtms = Type.StepClimber;
            }else if(machineTypeArray.get(3)){
                currentMachineTypeFtms = Type.StairClimber;
            }else if(machineTypeArray.get(4)){
                currentMachineTypeFtms = Type.Rower;
            }else if(machineTypeArray.get(5)){
                currentMachineTypeFtms = Type.IndoorBike;
            }
            if(currentMachineTypeFtms == null) currentMachineTypeFtms = Type.Treadmill;
            Helper.log("getServiceData=====Type"+currentMachineTypeFtms);
            return currentMachineTypeFtms;
        }
        return currentMachineTypeFtms;
    }

    public enum Type {
        Treadmill, CrossTrainer, StepClimber, StairClimber, Rower, IndoorBike, Other,
    }

    public interface ScanResultListener {
        boolean onScanResultResponse(ScanResult result);
    }

}


