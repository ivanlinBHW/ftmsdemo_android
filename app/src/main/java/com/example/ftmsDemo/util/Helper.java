package com.example.ftmsDemo.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

public class Helper {

    private final static boolean DEBUG = true;
    public static String version;

    public static void init(Activity a) {
        try {
            PackageInfo pInfo = a.getPackageManager().getPackageInfo(a.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void toast(Context c, String msg) {
        if (c == null)
            return;
        Toast.makeText(c, msg, Toast.LENGTH_SHORT)
                .show();
    }

    public static void log(String msg) {
        if (!DEBUG || msg == null)
            return;
        Log.d("DEBUG",version+msg);
    }


}