package com.example.ftmsDemo;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.le.ScanResult;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.ftmsDemo.adapter.BleDeviceAdapter;
import com.example.ftmsDemo.util.BleDevice;
import com.example.ftmsDemo.util.BleManager;
import com.example.ftmsDemo.util.ConnectManager;
import com.example.ftmsDemo.util.Helper;


public class MainActivity extends AppCompatActivity {

    private View scan_btn, stop_scan_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Helper.init(this);
        ListView list = findViewById(R.id.list);
        scan_btn = findViewById(R.id.scan);
        stop_scan_btn = findViewById(R.id.stop_scan);
        BleDeviceAdapter bleDeviceAdapter = new BleDeviceAdapter(MainActivity.this, BleManager.bleScanDeviceListData, BleManager.FilterType.FITNESS_MACHINE_SERVICE, false);
        BleManager.getInstance().init(MainActivity.this, bleDeviceAdapter, new BleManager.ScanResultListener() {
            @Override
            public boolean onScanResultResponse(ScanResult result) {
                return false;
            }
        }, BleManager.FilterType.FITNESS_MACHINE_SERVICE);

        scan_btn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                BleManager.getInstance().blueToothScan();
            }
        });

        stop_scan_btn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                BleManager.getInstance().stopScanning();
            }
        });
        
        list.setAdapter(bleDeviceAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(BleManager.bleScanDeviceListData.get(position).getDeviceTypeFtms() == BleManager.Type.IndoorBike){
                    toBleConnectActivity(BleManager.bleScanDeviceListData.get(position));
                }
            }
        });
    }

    private void toBleConnectActivity(BleDevice result){
        ConnectManager.currentDevice = result;
        Intent intent = new Intent(MainActivity.this, ConnectedActivity.class);
        startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(requestCode == BleManager.REQUEST_ACCESS_COURSE_LOCATION){
            BleManager.getInstance().startbt();
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BleManager.getInstance().stopScanning();
    }
}