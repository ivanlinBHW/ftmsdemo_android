package com.example.ftmsDemo.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;


import com.example.ftmsDemo.R;
import com.example.ftmsDemo.util.BleDevice;
import com.example.ftmsDemo.util.BleManager;

import java.util.List;

public class BleDeviceAdapter extends ArrayAdapter<BleDevice> {

    private List<BleDevice> list;
    private final Activity context;
    private BleManager.FilterType currentType;
    private boolean editable;


    public BleDeviceAdapter(@NonNull Activity context, @NonNull List<BleDevice> list, BleManager.FilterType type, boolean editable) {
        super(context, R.layout.li_data_ble_device, list);
        currentType = type;
        this.context = context;
        this.list = list;
        this.editable = editable;
    }


    public void setData(List<BleDevice> list) {
        this.list = list;
        notifyDataSetInvalidated();
    }

    public List<BleDevice>  getData() {
        return this.list;
    }

    public void removeData(BleDevice data) {
        this.list.remove(data);
        notifyDataSetInvalidated();
    }

    public void addData(int pos, BleDevice data) {
        this.list.add(pos, data);
        notifyDataSetInvalidated();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView name, address, rssi, info;
        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            convertView = inflator.inflate(R.layout.li_data_ble_device, null);

        }
        name = convertView.findViewById(R.id.txt_title);
        name.setText(list.get(position).getDisplayName());
        address = convertView.findViewById(R.id.txt_status);
        if(list.get(position).getScanResult().getDevice()!=null) {
            address.setText("Name: " + list.get(position).getScanResult().getDevice().getAddress());
        }else{
            address.setText("Name:  not found");

        }
        rssi = convertView.findViewById(R.id.txt_rssi);
        rssi.setText(String.valueOf(list.get(position).getScanResult().getRssi()));
        return convertView;
    }
}
